import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
})
export class LoginComponent implements OnInit {
  username: FormControl;
  password: FormControl;
  loginForm: FormGroup;
  error: string;

  constructor(private loginService: LoginService) {
    this.username = new FormControl('', [
      Validators.email,
      Validators.required,
    ]);
    this.password = new FormControl('', [
      Validators.required,
      this.ansiValidator,
    ]);
    this.loginForm = new FormGroup({
      username: this.username,
      password: this.password,
    });

    this.error = '';
  }

  ngOnInit(): void {}

  onSubmit() {
    this.loginService.login(this.username.value, this.password.value).subscribe(
      (response) => {
        console.log('Logged in!', response);
        this.error = '';
        window.location.href = 'http://onecause.com';
      },
      (error) => {
        console.log(error);
        this.error = error.error.error;
      }
    );
  }

  ansiValidator(control: AbstractControl): { [key: string]: any } | null {
    let ansi = /^[ -~\t\n\r]+$/;
    if (control.value && !ansi.test(control.value)) {
      return { notAnsi: true };
    }

    return null;
  }

  getErrorMessageUsername(): string {
    if (this.username.hasError('required')) {
      return 'You must enter a value';
    }

    return this.username.hasError('email') ? 'Not a valid email' : '';
  }

  getErrorMessagePassword(): string {
    if (this.password.hasError('required')) {
      return 'You must enter a value';
    }

    return this.password.hasError('notAnsi')
      ? 'Invalid characters in password'
      : '';
  }
}
