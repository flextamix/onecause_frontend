import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import UserCredentials from './user-credentials';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    let body: UserCredentials = {
      username: username,
      password: password,
      token: this.createToken(),
    };
    return this.http.post('/api/v1/login', body);
  }

  private createToken(): string {
    let now = new Date();
    let tokenString: string = now.getHours().toString();
    let minutes = now.getMinutes()
    tokenString += (minutes < 10) ? '0' + minutes : minutes.toString();
    return tokenString;
  }
}
