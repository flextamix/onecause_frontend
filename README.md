# OneCause Programming Exercise - Frontend

Author - David Mast

This frontend is written for my application to OneCause. The goal of this portion of the exercise is to present a view for the user to login to the restricted portion of the website. 

## Reflections

There were a few things I would have liked to do if I had had more time: 
* I would have implented a JWT or otherwise cookie based authentication scheme in order to keep users out of restricted portions of the webpage. 
* I would have made the webpage look more presentable; I'm new to Material and didn't have much time to look into using it throughout. 
* I would have written tests for the component and service that I added. 
* Add a Bitbucket Pipelines file to run tests on commits being pushed

## Running

This application can be run via either `ng serve` or via a Docker container:
* `docker build -t onecause-frontend`
* `docker run -p 80:80 --name onecause-frontend onecause-frontend:latest`

If running locally, it expects the backend server at `http://localhost:8080`, if running via Docker it will expect the backend server at `http://host.docker.internal:8080`