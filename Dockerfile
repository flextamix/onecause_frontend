FROM node:14.15.3-alpine AS compile-image

RUN npm install

WORKDIR /app
COPY package.json  .
RUN npm install

ENV PATH="./node_modules/.bin:$PATH" 

COPY . .
RUN ng build --prod

FROM nginx
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY --from=compile-image /app/dist/onecause-frontend /usr/share/nginx/html